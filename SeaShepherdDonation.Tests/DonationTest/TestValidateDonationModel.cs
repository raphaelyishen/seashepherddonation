﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeaShepherdBusinessLibrary;

namespace SeaShepherdDonation.Tests.DonationTest
{
    /// <summary>
    /// Unit test for ValidateDonationModel method
    /// </summary>
    [TestClass]
    public class TestValidateDonationModel
    {
        [TestMethod]
        public void FullDonationInformationShouldBeValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(string.Empty, error);
        }
        [TestMethod]
        public void TotalAmountLessthenZeroNotValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = -10;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Donation amount is not valid.", error);
        }
        [TestMethod]
        public void TotalAmountZeroNotValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 0;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Donation amount is not valid.", error);
        }
        [TestMethod]
        public void EmailNullNotValid()
        {
            // Arrange
            string email = null;
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Customer email is mandatory.", error);
        }
        [TestMethod]
        public void EmailOutofRangeNotValid()
        {
            // Arrange
            string email = "textisover50textisover50textisover50textisover50textisover50textisover50textisover50textisover50textisover50textisover50";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Customer information length is over limit.", error);
        }
        [TestMethod]
        public void PhoneNullValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = null;
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(string.Empty, error);
        }
        [TestMethod]
        public void FirstNameNullValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = null;
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(string.Empty, error);
        }
        [TestMethod]
        public void LastNameNullValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = null;
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsTrue(result);
            Assert.AreEqual(string.Empty, error);
        }
        [TestMethod]
        public void PhoneOutofRangeNotValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "045258081104525808110452580811045258081104525808110452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Customer information length is over limit.", error);
        }
        [TestMethod]
        public void SecuredCardDataNullNotValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = null;
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Card data information has encountered an issue. Please contact Sea Shepherd.", error);
        }
        [TestMethod]
        public void SecuredCardDataOutofRangeNotValid()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover" +
                "500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500t" +
                "extisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover" +
                "500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Card data information has encountered an issue. Please contact Sea Shepherd.", error);
        }
        [TestMethod]
        public void CombineNotValidErrors()
        {
            // Arrange
            string email = null;
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover" +
                "500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500t" +
                "extisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover" +
                "500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500textisover500";
            string error;

            // Act
            bool result = BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out error);

            // Assert
            Assert.IsFalse(result);
            Assert.AreEqual("Customer email is mandatory.Card data information has encountered an issue. Please contact Sea Shepherd.", error);
        }
    }
}
