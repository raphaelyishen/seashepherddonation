﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeaShepherdDonation.Controllers;
using SeaShepherdDonation.Models.Donation;
using Moq;
using Moq.Protected;
using System.Threading;
using System.Net;
using Newtonsoft.Json;

namespace SeaShepherdDonation.Tests.Controllers
{
    [TestClass]
    public class TestProcessDonationAsync
    {
        public static T GetValueFromJsonResult<T>(JsonResult jsonResult, string propertyName)
        {
            var property =
                jsonResult.Data.GetType().GetProperties()
                .Where(p => string.Compare(p.Name, propertyName) == 0)
                .FirstOrDefault();

            if (null == property)
                throw new ArgumentException("propertyName not found", "propertyName");
            return (T)property.GetValue(jsonResult.Data, null);
        }
        [TestMethod]
        public async Task FullCustomerdetailPaymentSecureDataExpiredShowError()
        {
            // Arrange
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";

            var controller = new DonationController();
            // Act
            var result = await controller.ProcessDonationAsync(email, phone, firstName, lastName, totalAmount, securedCardData);
            var serverResponseResult = GetValueFromJsonResult<ServerResponseModel>(result, "serverResponse");
            // Assert
            Assert.IsFalse(serverResponseResult.TransactionStatus);
            Assert.AreEqual("V6149,V6021,V6101,V6101,V6102,V6102,V6023", serverResponseResult.ValidationError);
        }
        [TestMethod]
        public async Task DataValidationFailedShowValidationError()
        {
            // Arrange
            string email = null;
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";
            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";

            var controller = new DonationController();
            // Act
            var result = await controller.ProcessDonationAsync(email, phone, firstName, lastName, totalAmount, securedCardData);
            var serverResponseResult = GetValueFromJsonResult<ServerResponseModel>(result, "serverResponse");
            // Assert
            Assert.IsFalse(serverResponseResult.TransactionStatus);
            Assert.AreEqual("Customer email is mandatory.", serverResponseResult.ValidationError);
        }

    }
}
