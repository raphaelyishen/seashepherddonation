﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeaShepherdDonation.Controllers;
using SeaShepherdDonation.Models.Donation;
using Moq;
using Moq.Protected;
using System.Threading;
using System.Net;
using Newtonsoft.Json;
using SeaShepherdBusinessLibrary;

namespace SeaShepherdDonation.Tests.DonationTest
{
    /// <summary>
    /// Unit test for SendDonationToEwayAsync method
    /// </summary>
    [TestClass]
    public class TestSendDonationToEwayAsync
    {
        private TransactionRequestModel PrepareTransactionrequestModel()
        {
            string email = "abc@gmail.com";
            string phone = "0452580811";
            string firstName = "testFirstName";
            string lastName = "testLastName";

            int totalAmount = 120;
            string securedCardData = "F9802Gh60NUHThn1Vu3FLnMwVY8viFhsGGvvUW-3q0VDk1tKy57wmQSb3EbOQOmj02fSS";
            return new TransactionRequestModel(new CustomerModel(email, phone, firstName, lastName), new PaymentModel(totalAmount), Enum.GetName(typeof(PaymentMethod), PaymentMethod.ProcessPayment),
                        Enum.GetName(typeof(TransactionEnumType), TransactionEnumType.Purchase), securedCardData);
        }
        [TestMethod]
        public async Task HttpRequestSuccessfulReturnValidResult()
        {
            // Arrange
            string jsonResult = "{\"ResponseMessage\":\"A2000\",\"TransactionID\":19585870,\"TransactionStatus\":true,\"Errors\":null}";
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.OK,
               Content = new StringContent(jsonResult),
           })
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsTrue(result.TransactionStatus);
            Assert.AreEqual(null, result.ValidationError);
            Assert.AreEqual("A2000", result.ResponseMessage);
            Assert.AreEqual("19585870", result.TransactionId);
        }
        [TestMethod]
        public async Task HttpBadRequestNotSuccessShowError()
        {
            // Arrange
            string jsonResult = "{\"ResponseMessage\":\"A2000\",\"TransactionID\":19585870,\"TransactionStatus\":true,\"Errors\":null}";
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.BadRequest,
               Content = new StringContent(jsonResult),
           })
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("Http request to eWAY encountered failure.", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
        [TestMethod]
        public async Task HttpRequestResponseContentIsNullShowError()
        {
            // Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.OK,
               Content = new StringContent(""),
           })
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("No result is returned from eWAY.", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
        [TestMethod]
        public async Task HttpRequestSuccessEWAYErrors()
        {
            // Arrange
            string jsonResult = "{\"ResponseMessage\":\"A2000\",\"TransactionID\":19585870,\"TransactionStatus\":true,\"Errors\": \"V6023,V6033\"}";
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.OK,
               Content = new StringContent(jsonResult),
           })
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("V6023,V6033", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
        [TestMethod]
        public async Task HttpRequestSuccessEWAYDecline()
        {
            // Arrange
            string jsonResult = "{\"ResponseMessage\":\"D4405\",\"TransactionID\":19585870,\"TransactionStatus\":false,\"Errors\": null}";
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.OK,
               Content = new StringContent(jsonResult),
           })
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual(null, result.ValidationError);
            Assert.AreEqual("D4405", result.ResponseMessage);
            Assert.AreEqual("19585870", result.TransactionId);
        }
        [TestMethod]
        public async Task HttpExceptionShowError()
        {
            // Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .Throws(new HttpRequestException())
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("Http request exception detected.", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
        [TestMethod]
        public async Task WebExceptionError()
        {
            // Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .Throws(new WebException())
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("Web request exception detected.", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
        [TestMethod]
        public async Task ExceptionError()
        {
            // Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
            // Setup the PROTECTED method to mock
           .Setup<Task<HttpResponseMessage>>(
              "SendAsync",
              ItExpr.IsAny<HttpRequestMessage>(),
              ItExpr.IsAny<CancellationToken>()
           )
           // prepare the expected response of the mocked http call
           .Throws(new Exception("This is a general exception."))
           .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object);
            TransactionRequestModel requestModel = PrepareTransactionrequestModel();
            var controller = new DonationController();

            // Act
            var result = await controller.SendDonationToEwayAsync(requestModel, httpClient);
            // Assert
            Assert.IsFalse(result.TransactionStatus);
            Assert.AreEqual("Exception detected. This is a general exception.", result.ValidationError);
            Assert.AreEqual(null, result.ResponseMessage);
            Assert.AreEqual(null, result.TransactionId);
        }
    }
}
