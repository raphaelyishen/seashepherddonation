﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeaShepherdBusinessLibrary;

namespace SeaShepherdDonation.Tests.DonationTest
{
    /// <summary>
    /// Unit test for StringLengthIsinRange method
    /// </summary>
    [TestClass]
    public class TestStringLengthIsinRange
    {
        [TestMethod]
        public void NotNullStringIsinRange()
        {
            // Arrange
            string s = "abc";
            int len = 4;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void NotNullStringIsNotinRange()
        {
            // Arrange
            string s = "abc";
            int len = 2;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void NullStringIsinRange()
        {
            // Arrange
            string s = null;
            int len = 2;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void NullStringRangeZeroInRange()
        {
            // Arrange
            string s = null;
            int len = 0;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void NullStringRangeNagtiveNotInRange()
        {
            // Arrange
            string s = null;
            int len = -2;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void NotNullStringRangeNagtiveNotInRange()
        {
            // Arrange
            string s = "abc";
            int len = -2;

            // Act
            bool result = BusinessLibrary.StringLengthIsinRange(s, len);

            // Assert
            Assert.IsFalse(result);
        }
    }
}
