﻿using System;

namespace SeaShepherdBusinessLibrary
{
    public static class BusinessLibrary
    {
        /// <summary>
        /// Very basic static compare function
        /// return false if string length is larger than provided maxlength.
        /// </summary>
        /// <param name="s">string being compared</param>
        /// <param name="len">max length that this string can have</param>
        /// <returns></returns>
        public static bool StringLengthIsinRange(this string s, int len)
        {
            if (len < 0)
                return false;
            else if (string.IsNullOrEmpty(s))
                return true;
            return s?.Length <= len;
        }
        /// <summary>
        /// We have clientside validation, but we will need to do server validation as well 
        /// so that to confirm our data is in correct format
        /// Checking if the required fields are not null
        /// Checking if fields answer has exceeded eWAY's API max length
        /// </summary>
        /// <returns>true if passed validation, false if failed.</returns>
        public static bool ValidateDonationModel(string email, string phone, string firstName, string lastName, int totalAmount, string securedCardData, out string error)
        {
            error = string.Empty;
            if (totalAmount <= 0)
            {
                error += "Donation amount is not valid.";
            }
            else if (!StringLengthIsinRange(totalAmount.ToString(), (int)PropertyMaxLength.TotalAmount))
            {
                error += "Donation amount length is over limit.";
            }

            if (email == null)
            {
                error += "Customer email is mandatory.";
            }

            if (email != null && !StringLengthIsinRange(email, (int)PropertyMaxLength.Email) ||
                (phone != null && !StringLengthIsinRange(phone, (int)PropertyMaxLength.Phone)) ||
                (firstName != null && !StringLengthIsinRange(firstName, (int)PropertyMaxLength.FirstName)) ||
                (lastName != null && !StringLengthIsinRange(lastName, (int)PropertyMaxLength.LastName)))
            {
                error += "Customer information length is over limit.";
            }

            if (securedCardData == null || !StringLengthIsinRange(securedCardData, (int)PropertyMaxLength.SecuredCardData))
            {
                error += "Card data information has encountered an issue. Please contact Sea Shepherd.";
            }
            //return false if there is any error
            if(error != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
