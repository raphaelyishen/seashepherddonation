﻿namespace SeaShepherdBusinessLibrary
{
    /// <summary>
    /// Constants class for storing required static data.
    /// </summary>
    public static class DonationConstants
    {
        public const string sandboxAPIKey = "F9802CL6tBYFeInWkTdR/ZTnii9NgHEz7fZNVCrWf0FSTnfXlMBxdM0lgJD5QSYSoOqUlI";
        public const string userPassword = "Abcd1234";
        public const string eWAYRequestURI = "https://api.sandbox.ewaypayments.com/Transaction";
    }
    public enum PaymentMethod
    {
        ProcessPayment,
        Authorise,
        TokenPayment,
        CreateTokenCustomer,
        UpdateTokenCustomer
    }
    public enum TransactionEnumType
    {
        Purchase,
        MOTO,
        Recurring
    }
    public enum PropertyMaxLength
    {
        FirstName = 50,
        LastName = 50,
        Email = 50,
        Phone = 50,
        SecuredCardData = 500,
        TotalAmount = 10
    }
}
