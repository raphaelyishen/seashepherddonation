﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SeaShepherdBusinessLibrary;
using SeaShepherdDonation.Models.Donation;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Mvc;

namespace SeaShepherdDonation.Controllers
{
    public class DonationController : Controller
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //return home page
        public ActionResult Index()
        {
            _logger.Info("Index page load.");
            return View();
        }
        /// <summary>
        /// Core function for donation program
        /// receiving data from clientside and generate the transaction request model
        /// sending the request and return server response as JSON
        /// </summary>
        /// <param name="customer">CustomerModel data</param>
        /// <param name="payment">PaymentModel data</param>
        /// <param name="securedCardData">SecuredCardData passed as string</param>
        /// <returns>return serverResponse object as JSON</returns>
        public async System.Threading.Tasks.Task<JsonResult> ProcessDonationAsync(string email, string phone, string firstName, string lastName, int totalAmount, string securedCardData)
        {
            _logger.DebugFormat("ProcessDonationAsync load by email : {0}, phone : {1}, firstName : {2}, lastName : {3}, totalAmount : {4}, securedCardData : {5} ", email, phone, firstName, lastName, totalAmount, securedCardData);
            string validationError;
            ServerResponseModel serverResponse = new ServerResponseModel();            
                //Do server side validation for all data
                if (BusinessLibrary.ValidateDonationModel(email, phone, firstName, lastName, totalAmount, securedCardData, out validationError))
                {
                    _logger.Info("Passed data validation.");
                    HttpClient httpClient = new HttpClient();
                    TransactionRequestModel requestModel = new TransactionRequestModel(
                        new CustomerModel(email, phone, firstName, lastName), new PaymentModel(totalAmount),
                        Enum.GetName(typeof(PaymentMethod), PaymentMethod.ProcessPayment),
                        Enum.GetName(typeof(TransactionEnumType), TransactionEnumType.Purchase), securedCardData);
                    serverResponse = await SendDonationToEwayAsync(requestModel, httpClient);
                }
                else
                {
                    _logger.Info("Failed data validation. Error: "+ validationError);
                    serverResponse.ValidationError = validationError;
                }

            return Json(new {serverResponse = serverResponse }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Async function to call eWAY web API
        /// with customer's details and securedCardData and make the transaction
        /// </summary>
        /// <param name="requestModel">Customer API request data model</param>
        /// <returns>ServerResponseModel containing status, message, transactionID etc.</returns>
        public async System.Threading.Tasks.Task<ServerResponseModel> SendDonationToEwayAsync(TransactionRequestModel requestModel, HttpClient httpClient)
        {
            _logger.DebugFormat("SendDonationToEwayAsync load by TransactionRequestModel : {0} ", requestModel);
            ServerResponseModel serverResponse = new ServerResponseModel();
            try
            {
                //Initiating the API request
                string base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(DonationConstants.sandboxAPIKey+":"+DonationConstants.userPassword));
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64authorization);
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, DonationConstants.eWAYRequestURI))
                {
                    request.Content = new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.Info("HttpRequest has success status response.");
                        HttpContent content = response.Content;
                        //Reading JSON result from eWAY
                        string result = await content.ReadAsStringAsync();
                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            JObject JSONObj = (JObject)JsonConvert.DeserializeObject(result);
                            string errors = JSONObj["Errors"].Value<string>();
                            if (String.IsNullOrEmpty(errors))
                            {
                                _logger.Info("Request to eWAY API is successful.");
                                if (JSONObj["TransactionStatus"] != null)
                                {
                                    serverResponse.TransactionStatus = JSONObj["TransactionStatus"].Value<bool>();
                                }
                                serverResponse.TransactionId = JSONObj["TransactionID"].Value<string>();
                                serverResponse.ResponseMessage = JSONObj["ResponseMessage"].Value<string>();
                                _logger.Info("TransactionId :"+ serverResponse.TransactionId + "ResponseMessage:" + serverResponse.ResponseMessage);
                            }
                            else
                            {
                                _logger.Info("Request to eWAY API has errors."+errors);
                                serverResponse.ValidationError = errors;
                            }
                        }
                        else
                        {
                            _logger.Info("No result returned from eWAY API.");
                            serverResponse.ValidationError = "No result is returned from eWAY.";
                        }
                    }
                    else
                    {
                        _logger.Info("HttpRequest failed.");
                        serverResponse.ValidationError = "Http request to eWAY encountered failure.";
                    }                   
                }
            }
            catch (HttpRequestException e)
            {
                serverResponse.ValidationError = "Http request exception detected.";
                _logger.Error(e.Message);
            }
            catch (WebException e)
            {
                serverResponse.ValidationError = "Web request exception detected.";
                _logger.Error(e.Message);
            }
            catch (Exception ex)
            {
                serverResponse.ValidationError = "Exception detected. " + ex.Message;
                _logger.Error(ex.Message);
            }
            return serverResponse;
        }
    }
}
