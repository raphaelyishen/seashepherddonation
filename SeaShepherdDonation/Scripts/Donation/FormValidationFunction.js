﻿//show warning message base on validation/server response
var showWarningMessage = function (message, transactionId) {
    var errorMessage;
    if (!$("#warningMessage").hasClass("alert-danger")) {
        $("#warningMessage").addClass("alert-danger")
    }
    if (!$("#warningMessage").hasClass("alert-success")) {
        $("#warningMessage").removeClass("alert-success")
    }
    if (transactionId) {
        errorMessage = "Transaction status: Failure. Transaction ID: " + transactionId + ". Error Message: " + message + ".";
    }
    else {
        errorMessage = "Transaction status: Failure. Error Message: " + message + ".";
    }
    $("#warningMessageText").text(errorMessage);
    $("#warningMessage").show();
    $("#DonationForm").hide();
    $("#thankYouPage").show();
};
//show success message base on server response
var showSuccessMessage = function (message) {
    if (!$("#warningMessage").hasClass("alert-danger")) {
        $("#warningMessage").removeClass("alert-danger")
    }
    if (!$("#warningMessage").hasClass("alert-success")) {
        $("#warningMessage").addClass("alert-success")
    }
    $("#warningMessageText").text("Transaction status: Success. Transaction ID: " + message + ".");
    $("#warningMessage").show();
    $("#DonationForm").hide();
    $("#thankYouPage").show();
};
//client side card detail validation function. Show warning message div if fail validation. 
var validateCardDetails = function () {
    showCardDetailError("name");
    showCardDetailError("card");
    showCardDetailError("expiry");
    showCardDetailError("cvn");
    if (!document.getElementById("securefieldcode").value) {
        return null;
    }
    else if ($("#nameError").is(":visible") || $("#cardError").is(":visible")
        || $("#expiryError").is(":visible") || $("#cvnError").is(":visible")) {
        return null;
    }
    else {
        return document.getElementById("securefieldcode").value;
    }
};
//check if the field has been successfully updated by callback
var showCardDetailError = function (fieldname) {
    var cardvaluefield = document.getElementById("securefieldcode" + fieldname);
    if (cardvaluefield.value === "false") {
        errorMessageDiv = $("body").find("#" + fieldname + "Error");
        var errorMessageTextDiv = $("body").find("#" + fieldname + "ErrorText");
        errorMessageTextDiv.text("Card " + fieldname + " is required.");
        errorMessageDiv.show();
    }
}