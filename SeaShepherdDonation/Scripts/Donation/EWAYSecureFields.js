﻿//EWAY secure fields' config, callback function.
var publicApiKey = DonationConstants.PUBLICAPI_KEY.SecureFieldConfigKey;
var fieldStyles = "line-height: 1; height: 34px; border: 1px solid #AAA; color: #000; padding: 2px;";

var nameFieldConfig = {
    publicApiKey: publicApiKey,
    fieldDivId: "eway-secure-field-name",
    fieldType: "name",
    styles: fieldStyles
};
var cardFieldConfig = {
    publicApiKey: publicApiKey,
    fieldDivId: "eway-secure-field-card",
    fieldType: "card",
    styles: fieldStyles,
    maskValues: false
};
var expiryFieldConfig = {
    publicApiKey: publicApiKey,
    fieldDivId: "eway-secure-field-expiry",
    fieldType: "expiry",
    styles: fieldStyles
};
var cvnFieldConfig = {
    publicApiKey: publicApiKey,
    fieldDivId: "eway-secure-field-cvn",
    fieldType: "cvn",
    styles: fieldStyles
};
function secureFieldCallback(event) {
    var errorMessageDiv = $("body").find("#" + event.targetField + "Error");
    var errorMessageTextDiv = $("body").find("#" + event.targetField + "ErrorText");
    // we need these individual fields to determine if the target field has been updated by callback function
    var secureIndividualField = document.getElementById("securefieldcode" + event.targetField);
    if (!event.fieldValid) {
        secureIndividualField.value = "false";
        errorMessageTextDiv.text(event.targetField + " did not load correctly. Error:" + event.errors);
        errorMessageDiv.show();
    }
    else if (!event.valueIsValid) {
        secureIndividualField.value = "false";
        errorMessageTextDiv.text(event.targetField + " field is not valid.");
        errorMessageDiv.show();
    }
    else if (!event.valueIsSaved) {
        secureIndividualField.value = "false";
        errorMessageTextDiv.text("Please wait for " + event.targetField + " to be saved.");
        errorMessageDiv.show();
    }
    else {
        //individual field has been successfully saved
        secureIndividualField.value = "true";
        // store the secureFieldCode to hidden field
        var secureField = document.getElementById("securefieldcode");
        secureField.value = event.secureFieldCode;
        errorMessageDiv.hide();
    }
};