﻿$(function () {
    //load eWAY secure fields when page load.
    $("eway-secure-field-name").append(eWAY.setupSecureField(nameFieldConfig, secureFieldCallback.bind(this)));
    $("eway-secure-field-card").append(eWAY.setupSecureField(cardFieldConfig, secureFieldCallback.bind(this)));
    $("eway-secure-field-expiry").append(eWAY.setupSecureField(expiryFieldConfig, secureFieldCallback.bind(this)));
    $("eway-secure-field-cvn").append(eWAY.setupSecureField(cvnFieldConfig, secureFieldCallback.bind(this)));
    //generating ajax call when user click the donation button.
    $("#SubmitDonation").on("click", function () {
        $("#SubmitDonation").blur();
        if ($("#DonationForm").valid()) {
            //parse the totalAmount according to eWAY's rules. Australia: for example $27.00 will have total amount 2700.
            var totalAmount = parseFloat(document.getElementById("TotalAmount").value).toFixed(2);
            if (totalAmount) {
                totalAmount = parseInt(totalAmount * DonationConstants.COUNTRY_LOWEST_DENOMINATION.AUD);
                //validate the card details on client side (on top of eWAY's validation)
                var securedCardData = validateCardDetails();
                if (securedCardData) {
                    var DonationFormData = {
                        Email: document.getElementById("CustomerEmail").value,
                        Phone: document.getElementById("CustomerPhone").value,
                        FirstName: document.getElementById("CustomerFirstName").value,
                        LastName: document.getElementById("CustomerLastName").value,
                        TotalAmount: totalAmount,
                        SecuredCardData: securedCardData
                    }
                    var $this = $(this);
                    $this.button("loading");
                    //initiate ajax call
                    $.ajax({
                        type: "POST",
                        url: "/Donation/ProcessDonationAsync",
                        data: DonationFormData,
                        dataType: "json",
                        success: function (data) {
                            $this.button("reset");
                            if (data && data.serverResponse) {
                                //ValidationError contains server side validation error, exception message, error from EWAY API etc. 
                                if (data.serverResponse.ValidationError) {
                                    showWarningMessage(data.serverResponse.ValidationError);
                                }
                                else if (!data.serverResponse.TransactionStatus) {
                                    showWarningMessage(data.serverResponse.ResponseMessage, data.serverResponse.TransactionId);
                                }
                                else {
                                    showSuccessMessage(data.serverResponse.TransactionId);
                                }
                            }
                        },
                        error: function (xhr) {
                            $this.button("reset");
                            showWarningMessage(xhr.responseText);
                        }
                    });
                }
            }
            else {
                showWarningMessage("Donate amount is not valid. Please try again.");
            }
        };
    });
    //rules for donation form validation.
    $("#DonationForm").validate({
        rules: {
            CustomerEmail: {
                required: true,
                email: true
            },
            TotalAmount: {
                required: true,
                number: true,
                min: 0.01
            }
        },
        messages: {
            CustomerEmail: "Please enter a valid email address.",
            TotalAmount: {
                required: "Please enter donate amount."
            }
        }
    });
}); 