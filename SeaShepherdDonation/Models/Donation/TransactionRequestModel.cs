﻿namespace SeaShepherdDonation.Models.Donation
{
    public class TransactionRequestModel
    {
        public CustomerModel Customer { get; set; }
        public PaymentModel Payment { get; set; }
        public string Method { get; set; }
        public string TransactionType { get; set; }
        public string SecuredCardData { get; set; }
        public TransactionRequestModel(CustomerModel customer, PaymentModel payment, string method, string transactionType, string securedCardData)
        {
            Customer = customer;
            Payment = payment;
            Method = method;
            TransactionType = transactionType;
            SecuredCardData = securedCardData;
        }
    }
}