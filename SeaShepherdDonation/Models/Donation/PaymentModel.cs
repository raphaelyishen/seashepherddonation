﻿namespace SeaShepherdDonation.Models.Donation
{
    public class PaymentModel
    {
        public int TotalAmount { get; set; }
        public PaymentModel(int totalAmount)
        {
            TotalAmount = totalAmount;
        }
    }
}