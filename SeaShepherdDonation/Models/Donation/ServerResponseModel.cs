﻿namespace SeaShepherdDonation.Models.Donation
{
    public class ServerResponseModel
    {
        //Error message: server side validation error or eWAY returned error message
        public string ValidationError { get; set; }
        public bool TransactionStatus { get; set; }
        public string TransactionId { get; set; }
        public string ResponseMessage { get; set; }
    }
}