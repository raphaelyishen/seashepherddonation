﻿namespace SeaShepherdDonation.Models.Donation
{
    public class CustomerModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CustomerModel(string email, string phone, string firstName, string lastName)
        {
            Email = email;
            Phone = phone;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}